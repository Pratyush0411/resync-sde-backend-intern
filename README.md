# Employee Management Back-end Application

## Overview

A backend Application which allows employees to:


1. Login (sample for testing: username: sdit1234, password: yolo)
2. View list of all employees (private route)
3. Find a particular employee by id (private route)

Please note the database folder contains the json files for the schema. The files are only for a reference for testing, any changes made to the database will not be reflected in the files.

## APIs

\*\*\*running on localhost: 3000

1. **/auth/register**: Sign up as a user of the system by passing username and password as json object. (sample for testing: username: sdit1234, password: yolo)
2. **/auth/login**: Login if already registered by passing username and password as json object. A user specific token is generated which is used for verification in other private API routes.
3. **employee/getEmployees**: fetch the list of names of all employees
4. **employee/getEmployee/:e_id**: Fetch the details of the employee with the user specified employee ID.

## Testing:

Follow the steps in order below:

1. Sign up as an employee by making a POST request to **/auth/register** with a json object containing username and password/ use the sample login given.
2. Login to your accounnt by making a POST request to **/auth/login** with a json object containing email and password. A token is generated. Copy the token.
3. To make a GET call to **employee/getEmployees**, first paste the token in Header in the format "token" followed by the token. Then make the API call to get employees. Without authorisation, it will show 'You do not have access!' and not return the details.
4. Follow the same process as in step 3 to make a GET call to **employee/getEmployee** followed by the ID of the employee whoose details need to be retrived.

## Set-up

1. Clone the repository to your local system
2. Go the 'Backend' directory using 'cd backend'
3. Install the dependencies in package.json
4. run 'npm start'

## Tech Stack:

1. Node.js
2. Express.js
3. MongoDB (Atlas)

## Packages used

1. **bcrypt**: Hash the employee password before storing to database
2. **body-parser**: Parse incoming urls and json objects
3. **express**: Framework to help in creating APIs,
4. **jsonwebtoken**: Create and verify token during login,
5. **mongoose**: Communicate with MongoDB database,
6. **nodemon**: Auto refresh node server
