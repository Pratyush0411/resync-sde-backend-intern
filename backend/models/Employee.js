const mongoose = require("mongoose");

// basic schema for employee information
const EmployeeSchema = mongoose.Schema({
    e_id:{
        type: String,
        required: true
    },

    Name:{
        type: String,
        required: true
    },
    
    Salary: {
        type: Number,
        required: true
    },
    date:{
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Employee', EmployeeSchema)