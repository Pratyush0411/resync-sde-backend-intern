//server link url: mongodb+srv://indian:oWgRcaTg0iJtAd8F@cluster0.8jxlk.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

// Main Back end app
const express = require("express");

const app = express();

const mongoose = require("mongoose");

const loginRoutes = require("./routes/login");
const empRoutes = require ("./routes/employee");
const bodyParser = require("body-parser");

require('dotenv/config');
// Connect to db

mongoose.connect(
    process.env.DATABASE,
    {useNewUrlParser: true},
    ()=> {console.log("Connected to DB");}
    );


app.use(express.json());
//app.use(morgan("dev"));

// used for request parsing
app.use(bodyParser.urlencoded({ extended: false }));

//Middlewares for seperate URLS
app.use("/auth", loginRoutes);
app.use("/employee", empRoutes);

// handling unrouted urls (404 error)
app.all('*', function(req, res) {
    throw new Error("Bad request")
})

app.use(function(e, req, res, next) {
    if (e.message === "Bad request") {
        res.status(404).json({error: {msg: e.message, stack: e.stack}});
    }
});

// debugging purpose
app.get('/', (req, res)=>{
    res.send ("We are on home page")
});



//listen to  server

app.listen(3000);