const jwt = require ("jsonwebtoken");

// Function to validate the token

module.exports = function (req,res,next){
    const token = req.header('token');
    if (!token) return res.status(401).send ('No token provided');
    try{
        const verified = jwt.verify(token, process.env.TOKEN);
        // not needed but for debugging purposes
        req.user = verified; // returns the payload
        //**********
        next();
    }
    catch{
        res.status(400).send('Invalid token credentials !');
    }
};