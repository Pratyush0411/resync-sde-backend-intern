
const express = require ("express");

const Employee = require("../models/Employee");

const router = express.Router();
// token validation function for private routes
const auth = require('./authPrivate');


// GET LIST OF ALL EMPLOYEE NAMES
router.get('/getEmployees',auth, async(req,res)=>{
    try{
        const emps = await Employee.find();
        let empNames = emps.map((employee)=>{return employee.Name});
        res.json(empNames);
    }
    catch(err){
        res.json({message: err});

    }
});

// GET DETAIL OF SPECIFIC EMPLOYEE
router.get('/getEmployee/:e_id',auth, async(req,res)=>{
    try{
        const emp = await Employee.findOne({e_id: req.params.e_id});
        res.json(emp);
    }
    catch(err){
        res.json({message: err});

    }
});

// POST for creating the database (testing)
router.post('/register',auth, async(req,res)=>{

    const checkUser = await Employee.findOne({e_id: req.body.e_id});
    if (checkUser) return res.status(400).send("Employee already in database!");

    const employee = new Employee({
        e_id: req.body.e_id,
        Name: req.body.Name,
        Salary: req.body.Salary,
    });

    try{
        const savedEmp = await employee.save();
        res.send({user: savedEmp.id});
    }
    catch(err){
        res.status(400).send(err);
    }

});


module.exports = router
