const express = require ('express');
const User = require('../models/User');

const router = express.Router();

const bcrypt = require("bcryptjs");

const jwt = require ("jsonwebtoken");

router.get('/', (req,res)=> {
    res.send("We are on login page")
});

// POST LOGIN CREDENTIALS FOR TOKEN

router.post('/login', async(req, res)=>{
    // checking if user exists
    const login = await User.findOne({username: req.body.username});
    if (!login) return res.status(400).send("Invalid credentials");

    const correctPass = await bcrypt.compare(req.body.password, login.password);
    if (!correctPass) return res.status(400).send("Invalid credentials");


    // create and return a token

    const token = jwt.sign({id: login._id}, process.env.TOKEN);
    res.header('token', token).send(token);


});


// POST to create the database (testing)

router.post("/register", async(req, res)=>{
    //validation
    try{
        const checkUser = await User.findOne({username: req.body.username});
        if (checkUser) return res.status(400).send("User already in database!");
    }
    catch(err){
        res.status(400).send(err);
    }

    // hashing passwords
    const salt = await bcrypt.genSalt(10);
    const hashedPass = await bcrypt.hash(req.body.password, salt)

    const user = new User({
        username: req.body.username,
        password: hashedPass
    });

    try{
        const savedUser = await user.save();
        res.send({user: savedUser.id});
    }
    catch(err){
        res.status(400).send(err);
    }

})


module.exports = router
